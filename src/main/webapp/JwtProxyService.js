"use strict";

var JwtProxyService = (function(authorization_endpoint) {
    
    function tempIframe(url, params, onload) {
        var elt = document.createElement("iframe");
        elt.setAttribute("style", "display: none");
        elt.setAttribute("src", url + (params && params.length ? "?" + params.join('&') : ''));
        elt.onload = function() {
            onload(elt);
        };
        document.body.appendChild(elt);
    }
    
    function parseHash(hash) {
        var r = {};        
        hash.substring(1).split('&').forEach(function (s) {
            var m = s.match(/(.*?)=(.*)/);
            if (m) r[m[1]] = m[2];
        });
        return r;
    }

    function get(service, flags) {
        // TODO caching
        
        if (!flags) flags = {};

        var wanted_nonce = "" + Math.random();
        var auth_params = [
            "response_type=id_token",
            "prompt=none",
            "nonce=" + wanted_nonce,
            "client_id=" + encodeURIComponent(service),
            "redirect_uri=" + encodeURIComponent(document.location.href + "/empty.html"),
        ];

        if (!window.Promise) alert("Use Firefox or Chrome");
        
        return new Promise(function (resolve, reject) {
            tempIframe(authorization_endpoint, auth_params, function (elt) {
                var hash;
                try {
                    var hash = elt.contentWindow.location.hash;
                } catch (e) {
                    return reject("authorize failed");
                } finally {
                    elt.parentNode.removeChild(elt);
                }
                var params = parseHash(hash);

                if (!params || !params.id_token) {
                    reject("something bad happened");
                } else {
                    resolve(params.id_token);
                }
            })
        });
    }

    function getMulti(urls) {
        // TODO?: implement /tokens to get multiple JWTs at once
        return Promise.all(urls.map(get));
    }

    function parseJwt(jwt) {
        var l = jwt.split('.');
        return { header: JSON.parse(atob(l[0])), payload: JSON.parse(atob(l[1])), signature: l[2] };
    }

    function raw_ajax(url, params, id_token_jwt) {
        if (!params) params = {};
        params.headers = new Headers({ Authorization: "Bearer " + id_token_jwt });

        if (!window.fetch) alert("Use Firefox or Chrome");
        
        return fetch(url, params).then(service =>
             params.dataType === 'text' ? service.text() : service.json()
        );
    }

    function ajax(baseUrl, wsName, params, id_token_jwt) {
        var url = baseUrl + wsName;
        if (id_token_jwt) {
            return raw_ajax(url, params, id_token_jwt);
        } else {
            return get(baseUrl).then(id_token_jwt =>
                                     raw_ajax(url, params, id_token_jwt));
        }
    }
    
    return { get: get, getMulti: getMulti, parseJwt: parseJwt, ajax: ajax };
    
})("{{jwtProxyService_url}}/authorize");
