package jwtProxyService;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import org.jose4j.lang.JoseException;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.JsonWebKeySet;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jwk.RsaJsonWebKey;

import org.jasig.cas.client.util.CommonUtils;
import org.apache.commons.logging.LogFactory;

import static jwtProxyService.Utils.*;

@SuppressWarnings("serial")
public class Main extends HttpServlet {           
    Conf conf = null;
    Ldap ldap;
    RsaJsonWebKey rsaJsonWebKey;

    org.apache.commons.logging.Log log = LogFactory.getLog(Main.class);
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (conf == null) initConf(request);
        switch (request.getServletPath()) {
            case "/authorize": authorize(request, response); break;
            case "/.well-known/jwks.json":
            case "/jwks.json": jwks(request, response); break;
            case "/JwtProxyService.js":
            case "/widget-loader.js":
            case "/test/basic.html":
            case "/test/embed-widgets.html":
                nearly_static_file(request, response, conf, request.getServletPath()); break;
            default: response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    void authorize(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!"id_token".equals(request.getParameter("response_type"))) {
            bad_request(response, "only response_type=id_token allowed");
            return;
        }

        String uid = request.getRemoteUser();
        if (uid != null) {
            JwtClaims id_token = id_token(request);
            ldap.addClaims(id_token, uid);
            String redirect_uri = request.getParameter("redirect_uri");
            String state = request.getParameter("state");
            if (!allowRedirect(response, redirect_uri)) return;
            redirect_uri += "#id_token=" + signJwt(id_token.toJson());
            if (state != null) redirect_uri += "&state=" + urlencode(state);
            response.sendRedirect(redirect_uri);
        } else if (!hasParameter(request, "auth_checked")) {
            response.sendRedirect(cas_login(request, response, "none".equals(request.getParameter("prompt"))));
        } else {
            bad_request(response, "TODO");
        }
    }
        
    void jwks(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf8");
        response.getWriter().write(new JsonWebKeySet(rsaJsonWebKey).toJson(JsonWebKey.OutputControlLevel.PUBLIC_ONLY));
    }

    static void nearly_static_file(HttpServletRequest request, HttpServletResponse response, Conf conf, String file) throws ServletException, IOException {
        response.setContentType(guessContentTypeFromName(file));
        String content = file_get_contents(request, file);
        // mustache syntax
        content = content.replace("{{jwtProxyService_url}}", conf.jwtProxyService_url);
        response.getWriter().write(content);
    }

    private String cas_login(HttpServletRequest request, HttpServletResponse response, boolean gateway) throws IOException {
        // re-use code from CAS to reconstruct current URL
        // (nb: not using encode=true since it adds jsessionid in url which mess up things. we do not need it)
        String currentUrl = CommonUtils.constructServiceUrl(request, response, null, url2cas_serverName(conf.jwtProxyService_url), "service", "ticket", true);
        currentUrl += (request.getQueryString() == null ? "?" : "&") + "auth_checked=true";
        
        return CommonUtils.constructRedirectUrl(conf.cas_login_url, "service", currentUrl, false, gateway);        
    }
   
    private boolean allowRedirect(HttpServletResponse response, String redirect_uri) {
        if (conf.allowedOrigins != null) {
            for (String allowed : conf.allowedOrigins) {
                if (redirect_uri.startsWith(allowed + "/")) return true;
            }
        }
        String msg = "disallowing redirect_uri " + redirect_uri;
        log.error(msg);
        bad_request(response, msg);
        return false;
    }
    
    private JwtClaims id_token(HttpServletRequest request) {
        JwtClaims claims = new JwtClaims();
        claims.setIssuer(conf.jwtProxyService_url);
        claims.setAudience(request.getParameter("client_id"));
        claims.setExpirationTimeMinutesInTheFuture(conf.expirationTimeMinutes);
        //claims.setGeneratedJwtId(); // a unique identifier for the token
        claims.setIssuedAtToNow();  // when the token was issued/created (now)
        //claims.setNotBeforeMinutesInThePast(2); // time before which the token is not yet valid (2 minutes ago)
        claims.setSubject(request.getRemoteUser());

        String nonce = request.getParameter("nonce");
        if (nonce != null) claims.setStringClaim("nonce", nonce);
        
        return claims;
    }

    private String signJwt(String jwt) {
        try {
            return signJwt_(jwt);
        } catch (JoseException e) {
            log.error(e);
            return null;
        }
    }
    private String signJwt_(String jwt) throws JoseException {
        JsonWebSignature jws = new JsonWebSignature();

        jws.setPayload(jwt);
        jws.setKey(rsaJsonWebKey.getPrivateKey());
        jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

        return jws.getCompactSerialization();
    }
    
    synchronized void initConf(HttpServletRequest request) {
        ServletContext sc = request.getServletContext();
        conf = getConf(sc);
        ldap = new Ldap(conf.ldap);
        initRsaJsonWebKey();
    }

    synchronized void initRsaJsonWebKey() {
        try {
            rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);
            rsaJsonWebKey.setUse("sig");
            rsaJsonWebKey.setKeyId(java.util.UUID.randomUUID().toString());
        } catch (JoseException e) {
            log.error(e);
        }
    }

    static Conf getConf(ServletContext sc) {
        Gson gson = new Gson();
        Conf conf = gson.fromJson(getConf(sc, "config.json", true), Conf.class);
        conf.init();
        return conf;
    }

    static String getConf(ServletContext sc, String jsonFile, boolean mustExist) {
        String s = file_get_contents(sc, "WEB-INF/" + jsonFile, mustExist);
        if (s == null) return "{}"; // do not fail here, checks are done on required attrs
        // allow trailing commas
        s = s.replaceAll(",(\\s*[\\]}])", "$1");
        return s;
    }
        
}

