const Express = require('express');
const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
const cors = require('cors');

const issuer = "https://cas-test.univ-paris1.fr/JwtProxyService";
const origins = ['http://area51.univ-paris1.fr'];
const audience = "http://localhost:4001";

const app = new Express();
app.use(jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 2,
    jwksUri: `${issuer}/.well-known/jwks.json`
  }),
  audience: audience,
  issuer: issuer,
  algorithms: [ 'RS256' ]
}));

app.use(cors({ origin: origins }));

app.get('/', (req, res) => {
    res.send("Hello " + req.user.sub);
});

// Start the server.
const port = process.env.PORT || 4001;
app.listen(port, function(error) {
  if (error) {
    console.log(error);
  } else {
    console.log('Listening on http://localhost:' + port);
  }
});
