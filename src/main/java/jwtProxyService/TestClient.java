package jwtProxyService;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.jwk.HttpsJwks;
import org.jose4j.keys.resolvers.HttpsJwksVerificationKeyResolver;

import static jwtProxyService.Utils.*;

@SuppressWarnings("serial")
public class TestClient extends HttpServlet {
    Conf conf;
    JwtConsumer jwtConsumer;

    org.apache.commons.logging.Log log = Utils.log(TestClient.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (jwtConsumer == null) init(request);
        handleCrossOrigin(request, response);

        switch (request.getPathInfo()) {
        case "/hello": hello(request, response); break;
        case "/me": me(request, response); break;
        case "/": Main.nearly_static_file(request, response, conf, "/test/client/widget.html"); break;
        default: response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    public void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (jwtConsumer == null) init(request);
        handleCrossOrigin(request, response);
    }
    
    private void handleCrossOrigin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String origin = request.getHeader("Origin");
        if (origin == null) return;
        if (conf.allowedOrigins != null && conf.allowedOrigins.contains(origin)) {
	    response.setHeader("Access-Control-Allow-Origin", origin);
            response.addHeader("Access-Control-Allow-Headers", "Authorization");
        } else {
            String msg = "disallowing Cross Origin request from " + origin;
            log.error(msg);
            bad_request(response, msg);
        }
    }

    synchronized void init(HttpServletRequest request) {
        conf = Main.getConf(request.getServletContext());
        String issuer = conf.jwtProxyService_url;
        String jwks_uri = conf.jwtProxyService_url + "/jwks.json";
        
        HttpsJwks httpsJkws = new HttpsJwks(jwks_uri);
        HttpsJwksVerificationKeyResolver httpsJwksKeyResolver = new HttpsJwksVerificationKeyResolver(httpsJkws);

        jwtConsumer = new JwtConsumerBuilder()
            .setVerificationKeyResolver(httpsJwksKeyResolver)
            .setExpectedIssuer(issuer)
            .setExpectedAudience(conf.jwtProxyService_url + "/test/client")
            .build();
    }

    private JwtClaims getUser(HttpServletRequest request, HttpServletResponse response) throws IOException, InvalidJwtException {
        String auth = request.getHeader("Authorization");
        String jwt = auth != null ? removePrefixOrNull(auth, "Bearer") : null;
        if (jwt == null) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "missing Authorization Bearer");
            return null;
        }
        
        return jwtConsumer.processToClaims(jwt);
    }
    
    void hello(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            JwtClaims user = getUser(request, response);
            if (user != null) {
                response.getWriter().write("Hello " + user.getStringClaimValue("name"));
            }
        } catch (MalformedClaimException | InvalidJwtException e) {
            respond_json(response, e);
        }
    }

    void me(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            JwtClaims user = getUser(request, response);
            if (user != null) {
                respond_json(response, asMap("me", user.getStringClaimValue("sub")));
            }
        } catch (MalformedClaimException | InvalidJwtException e) {
            respond_json(response, e);
        }
    }    
}
