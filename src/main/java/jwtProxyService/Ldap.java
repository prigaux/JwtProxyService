package jwtProxyService;

import java.util.Collections;
import java.util.List;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.jose4j.jwt.JwtClaims;

import static jwtProxyService.Utils.asMap;
    
class Ldap {
    @SuppressWarnings("serial")
    static class Attrs extends HashMap<String, List<String>> {}

    static class LdapConf {
        String url, bindDN, bindPasswd, peopleDN;

        Map<String,String> toOIDC;
    }
    LdapConf ldapConf;
    DirContext dirContext;
    Log log = LogFactory.getLog(Ldap.class);

    Ldap(LdapConf ldapConf) {
        this.ldapConf = ldapConf;
    }
    
    private DirContext ldap_connect() {
        Map<String,String> env =
            asMap(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory")
             .add(Context.PROVIDER_URL, ldapConf.url)
             .add(Context.SECURITY_AUTHENTICATION, "simple")
             .add(Context.SECURITY_PRINCIPAL, ldapConf.bindDN)
             .add(Context.SECURITY_CREDENTIALS, ldapConf.bindPasswd);

        try {
            return new InitialDirContext(new Hashtable<>(env));
        } catch (NamingException e) {
            log.error("error connecting to ldap server", e);
            throw new RuntimeException("error connecting to ldap server");
        }
    }

    synchronized DirContext getDirContext() {
        if (dirContext == null) {
            dirContext = ldap_connect();
        }
        return dirContext;
    }

    @SuppressWarnings("unchecked")
    Map<String, String> addClaims(JwtClaims claims, String uid) {
        try {
            String name = "uid=" + uid + "," + ldapConf.peopleDN;
            Attributes attrs = getDirContext().getAttributes(name, ldapConf.toOIDC.keySet().toArray(new String[0]));
            Map<String,String> r = new HashMap<>();
            for (String attr: ldapConf.toOIDC.keySet()) {
                Attribute vals = attrs.get(attr);
                if (vals != null) {
                    String oidcAttr = ldapConf.toOIDC.get(attr);
                    if (oidcAttr.startsWith("[]")) {
                        oidcAttr = oidcAttr.substring(2);
                        claims.setClaim(oidcAttr, Collections.list((NamingEnumeration<String>) vals.getAll()));
                    } else {
                        claims.setClaim(oidcAttr, vals.get(0));
                    }
                }
            }
            return r;
        } catch (NamingException e) {
            log.error(e);
            return null;
        }
    }
}
