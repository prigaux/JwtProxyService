package jwtProxyService;


import java.util.Set;


class Conf {
    String cas_base_url;
    String jwtProxyService_url;
    Set<String> allowedOrigins;

    // below have valid default values
    Integer expirationTimeMinutes;
    String cas_login_url;
    String cas_logout_url;
    String ent_logout_url;

    Ldap.LdapConf ldap;
    
    Conf init() {
        if (cas_base_url == null) throw new RuntimeException("config.json must set cas_base_url");
        if (jwtProxyService_url == null) throw new RuntimeException("config.json must set jwtProxyService_url");
        if (cas_login_url == null) cas_login_url = cas_base_url + "/login";
        if (cas_logout_url == null) cas_logout_url = cas_base_url + "/logout";
        if (expirationTimeMinutes == null) expirationTimeMinutes = 60;
        return this;
    }
}
