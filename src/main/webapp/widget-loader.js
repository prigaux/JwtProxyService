(function () {
    "use strict";

    var ourUrl = "{{jwtProxyService_url}}/widget-loader.js";

    if (window.JwtProxyService) {
        main();
    } else {
        loadSript("{{jwtProxyService_url}}/JwtProxyService.js", function () {
            main();
        });
    }

    function main() {
        if (!JwtProxyService.widget)
            JwtProxyService.widget = widget_helpers();
        load();
    }

    function widget_helpers() {
        function div(widgetUrl) {
            return document.querySelectorAll("script[src='" + ourUrl + "']");
        }
        return { div: div };
    }
    
    function load() {
        var elts = [];
        for (let elt of document.querySelectorAll("script[src='" + ourUrl + "']")) {
            var state = elt.getAttribute("data-state");
            if (state !== 'loading' && state !== 'loaded') {
                elt.setAttribute("data-state", 'loading');
                elts.push(elt);
            }
        }
        var widgetUrls = elts.map(elt => elt.getAttribute("data-widget-url"));
    
        JwtProxyService.getMulti(widgetUrls).then(function (id_token_jwt_list) {
            widgetUrls.forEach(function (widgetUrl, i) {
                loadOne(elts[i], widgetUrl, id_token_jwt_list[i]);
            });
        });
    }

    function loadOne(elt, widgetUrl, id_token_jwt) {
        JwtProxyService.ajax(widgetUrl, "/", { dataType: 'text' }, id_token_jwt).then(html => {
            insertHTMLAfter(elt, html).setAttribute("data-widget-url", widgetUrl);
            elt.setAttribute("data-state", 'loaded');
        });
    }

    function loadSript(url, onload) {
        var script = document.createElement('script');
        script.src = url;
        script.onload = onload;
        document.head.appendChild(script);
    }
    
    function insertHTMLAfter(elt, html) {
        var div = document.createElement("div");
        div.innerHTML = html;
        insertAfter(elt, div);
        activateScriptsInOrder(toArray(div.getElementsByTagName("script")));
        return div;
    }

    function activateScriptsInOrder(scripts) {
        var script = scripts.shift();
        if (!script) return;
        var newScript = nodeScriptClone(script);
        newScript.onload = function () { activateScriptsInOrder(scripts) };
        replaceNode(script, newScript);
    }
    function insertAfter(node, elt) {
        node.parentNode.insertBefore(elt, node.nextSibling);
    }
    function replaceNode(current, newNode) {
        current.parentNode.replaceChild(newNode, current);
    }
    function nodeScriptClone(node) {
        var script = document.createElement("script");
        script.innerHTML = node.innerHTML;
        for (let attr of node.attributes) {
            script.setAttribute(attr.name, attr.value);
        }
        return script;
    }
    function toArray(l) {
        return [].slice.call(l);
    }

 })();
