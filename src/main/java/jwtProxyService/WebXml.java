package jwtProxyService;

import javax.servlet.*;

import org.jasig.cas.client.session.SingleSignOutFilter;
import org.jasig.cas.client.util.HttpServletRequestWrapperFilter;
import org.jasig.cas.client.validation.Cas20ProxyReceivingTicketValidationFilter;

import static jwtProxyService.Utils.*;

public class WebXml implements ServletContextListener {
        
    public void contextDestroyed(ServletContextEvent event) {}

    public void contextInitialized(ServletContextEvent event) {
        configure(event.getServletContext());
    }

    private void configure(ServletContext sc) {
        Conf conf = Main.getConf(sc);
                
        addFilter(sc, "CAS Single Sign Out", SingleSignOutFilter.class,
                  asMap("casServerUrlPrefix", conf.cas_base_url),
                  "/authorize");

        addFilter(sc, "CAS Validate", Cas20ProxyReceivingTicketValidationFilter.class,
                  asMap("casServerUrlPrefix", conf.cas_base_url)
                   .add("serverName", url2cas_serverName(conf.jwtProxyService_url))
                   .add("redirectAfterValidation", "false"), 
                  "/authorize");


        addFilter(sc, "CAS Request Wrapper", HttpServletRequestWrapperFilter.class, null,
                  "/authorize");

        addServlet(sc, "TestClient", TestClient.class, null, "/test/client/*");
        
        addServlet(sc, "JwtProxyService", Main.class, null, "/");
    }
}
